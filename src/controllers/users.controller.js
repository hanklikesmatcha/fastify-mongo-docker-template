const ObjectId = require("mongodb").ObjectId
const uuid = require('uuid')

async function listUsers(req, reply) {
  const users = this.mongo.db.collection("users")
  const result = await users.find({}).toArray()
  reply.send(result)
}

async function addUser(req, reply) {
  console.log('AAAAAAA',this.mongo)
  const users = this.mongo.db.collection("users")
  const { firstName, lastName  } = req.body
  const id = uuid.v4.toString
  const data = { id, firstName, lastName }
  const result = await users.insertOne(data)
  reply.code(201).send(result)
}

async function getUser(req, reply) {
    const users = this.mongo.db.collection("users");
    const result = await users.findOne({ _id: new ObjectId(req.params.id) });
    if (result) {
      return reply.send(result);
    }
    reply.code(500).send({ message: "Not found" });
}

module.exports = { listUsers, addUser, getUser };