
const { listUsers, addUser, getUser } = require("../controllers/users.controller");

async function routes(fastify, options) {
  fastify.get("/users", listUsers);
  fastify.post("/users", addUser);
  fastify.get("/users/:id", getUser);
}
module.exports = routes;