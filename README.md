## Start
1. Create `.env` in `src`


2. Run docker 
```
$ docker-compose build
```
```
$ docker-compose up
```

3. Create collections
```
$ docker exec -it taggun_mongodb_1 mongo -u root -p password
```
```
$ use backend_db // Switch to backend_db. Mongo will create the db if the db doesn't exist. 
```
```
$ db.createCollection('users') // There are two endpoints in this template require a collection called users
```

### API Endpints
1. **POST** `http://127.0.0.1:8080/users`

2. **GET** `http://127.0.0.1:8080/users/<user_id>`