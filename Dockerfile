FROM node:17
WORKDIR /app/src
COPY ./package.json /app/package.json
RUN npm install
EXPOSE 8080
COPY ./src .
CMD ["npm", "run", "dev"]